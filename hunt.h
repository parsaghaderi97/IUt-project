#ifndef HUNT_H
#define HUNT_H
#include<QObject>
#include<QGraphicsRectItem>
#include<QGraphicsScene>
#include<QTimer>
#include<QGraphicsPixmapItem>
#include<QPixmap>
#include"hunter.h"
class hunt:public QObject ,public QGraphicsPixmapItem
{
Q_OBJECT
public:
    hunt();
  //
public slots:
    void spawn();
};

#endif // HUNT_H
