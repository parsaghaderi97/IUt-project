#-------------------------------------------------
#
# Project created by QtCreator 2016-06-21T19:13:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FP
TEMPLATE = app


SOURCES += main.cpp \
    bird.cpp \
    birdi.cpp \
    baloone.cpp \
    hunt.cpp \
    hunter.cpp \
    target.cpp \
    gun.cpp \
    score.cpp \
    game.cpp

HEADERS  += \
    bird.h \
    birdi.h \
    baloone.h \
    hunt.h \
    hunter.h \
    target.h \
    gun.h \
    score.h \
    game.h

FORMS    += mainwindow.ui

RESOURCES += \
    pics.qrc
