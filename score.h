#ifndef SCORE_H
#define SCORE_H
#include<QGraphicsTextItem>
class score:public QGraphicsTextItem
{

public:
    score(QGraphicsItem *parent=0);
    void increaseScore();
private:
    int Score;
};

#endif // SCORE_H
